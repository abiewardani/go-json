module gitlab.com/tuxer/go-json

go 1.13

require (
	gitlab.com/tuxer/go-db v0.0.0-20191030051519-cd407f09d6db
	gitlab.com/tuxer/go-logger v0.0.0-20181117123619-174e86aaabc1
)
